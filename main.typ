#import "@preview/charged-ieee:0.1.0": ieee

#set page(
  paper: "iso-b7",
  width: 8.5in,
  height: 11in
)

#show: ieee.with(
  title: [An Empirically Verified Lower Bound for The Number Of Empty Pages Allowed In a SIGBOVIK Paper],
  abstract: [
    We show that at least one empty page is accepted at SIGBOVIK.
  ],
  authors: (
    (
      name: "Frans Skarman",
      organization: [Linköping University],
    ),
  ),
  bibliography: bibliography("main.bib")
)

Paper maximization is an important subject @Abrams2021, @Skarman2023. However, existing techniques requires effort from authors. A so-far unexplored technique is to simply add a sequence of blank pages inside the paper. In order to determine how effective this technique is, an upper bound on the number of blank pages SIGBOVIG will accept must be established. This work is the start of such an empirical study.

// Your content goes below.

#colbreak()
#colbreak()
#colbreak()
#colbreak()
